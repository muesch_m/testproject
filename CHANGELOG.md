## [2.2.2](https://gitlab.com/megamuesch99/testproject/compare/v2.2.1...v2.2.2) (2022-03-04)


### Bug Fixes

* Small bug ([858a2df](https://gitlab.com/megamuesch99/testproject/commit/858a2dfc9f9a59084708ceb90071cedc5d14eced))
* Small bug 2 ([3f50318](https://gitlab.com/megamuesch99/testproject/commit/3f503180c0e852c83abc3904e152077985dfb156))

## [2.2.1](https://gitlab.com/megamuesch99/testproject/compare/v2.2.0...v2.2.1) (2022-03-04)


### Bug Fixes

* Console text ([feb6a3b](https://gitlab.com/megamuesch99/testproject/commit/feb6a3be0c505d294a436e6144197b49667e2e77))

# [2.2.0](https://gitlab.com/megamuesch99/testproject/compare/v2.1.0...v2.2.0) (2022-03-04)


### Features

* Add changelog ([3dd0551](https://gitlab.com/megamuesch99/testproject/commit/3dd0551b067c6f00befb751afdf16b57a548106b))
